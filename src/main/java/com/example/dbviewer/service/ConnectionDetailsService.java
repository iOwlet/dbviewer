package com.example.dbviewer.service;

import com.example.dbviewer.entity.ConnectionDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ConnectionDetailsService {

    Page<ConnectionDetails> getConnectionList(Pageable pageable);

    ConnectionDetails getConnection(Long id);

    ConnectionDetails save(ConnectionDetails connectionDetails);

    ConnectionDetails update(Long id, ConnectionDetails connectionDetails);

    void delete(Long id);
}
