package com.example.dbviewer.service.impl;

import com.example.dbviewer.dto.ColumnDto;
import com.example.dbviewer.dto.SchemaDto;
import com.example.dbviewer.dto.TableDto;
import com.example.dbviewer.entity.ConnectionDetails;
import com.example.dbviewer.mapper.ResultSetMapper;
import com.example.dbviewer.service.ConnectionDetailsService;
import com.example.dbviewer.service.DbConnectionService;
import com.example.dbviewer.service.PasswordEnDecryptionService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
@Slf4j
public class DbConnectionServiceImpl implements DbConnectionService {

    public static final String SELECT_SIMPLE_DATA = "SELECT * FROM %s.%s LIMIT 10;";
    public static final String JDBC_POSTGRESQL_CONNECTION_STRING = "jdbc:postgresql://%s:%s/%s";

    private final ConnectionDetailsService connectionDetailsService;
    private final ResultSetMapper mapperService;
    private final PasswordEnDecryptionService passwordEnDecryptionService;

    @Override
    public List<SchemaDto> getSchemas(Long connectionId) {
        log.info("Get schemas for connectionId = {}", connectionId);
        List<SchemaDto> schemas = new ArrayList<>();
        try (Connection connection = getConnection(connectionId)) {
            DatabaseMetaData connectionMetaData = connection.getMetaData();
            ResultSet schemasData = connectionMetaData.getSchemas();
            while (schemasData.next()) {
                schemas.add(mapperService.mapSchemaDto(schemasData));
            }
        } catch (Exception exception) {
            log.error(exception.getMessage(), exception);
        }

        return schemas;
    }

    @Override
    public List<TableDto> getTables(Long connectionId, String schema) {
        log.info("Get tables for connectionId = {}; schema = {}", connectionId, schema);
        List<TableDto> tables = new ArrayList<>();
        try (Connection connection = getConnection(connectionId)) {
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet tablesData = metaData.getTables(null, schema, null, null);
            while (tablesData.next()) {
                tables.add(mapperService.mapTableDto(tablesData));
            }
        } catch (Exception exception) {
            log.error(exception.getMessage(), exception);
        }

        return tables;
    }

    @Override
    public List<ColumnDto> getColumns(Long connectionId, String schema, String table) {
        log.info("Get columns for connectionId = {}; schema = {}; table = {}", connectionId, schema, table);
        List<ColumnDto> columns = new ArrayList<>();
        try (Connection connection = getConnection(connectionId)) {
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet columnsData = metaData.getColumns(null, schema, table, null);
            while (columnsData.next()) {
                columns.add(mapperService.mapColumnDto(columnsData));
            }
        } catch (Exception exception) {
            log.error(exception.getMessage(), exception);
        }

        return columns;
    }

    @Override
    public List<Map<String, String>> getData(Long connectionId, String schema, String table) {
        log.info("Get simple data for connectionId = {}; schema = {}; table = {}", connectionId, schema, table);
        List<Map<String, String>> data = new ArrayList<>();
        try (Connection connection = getConnection(connectionId)) {
            Statement statement = connection.createStatement();

            String selectSimpleData = String.format(SELECT_SIMPLE_DATA, schema, table);
            ResultSet resultSet = statement.executeQuery(selectSimpleData);
            while (resultSet.next()) {
                data.add(mapperService.mapTableData(resultSet));
            }
        } catch (Exception exception) {
            log.error(exception.getMessage(), exception);
        }

        return data;
    }

    private Connection getConnection(Long connectionId) throws SQLException {
        ConnectionDetails connectionDetails = connectionDetailsService.getConnection(connectionId);

        String url = getDbUrl(connectionDetails);
        log.debug("Opening connection to {} with user {}", url, connectionDetails.getUsername());
        return DriverManager.getConnection(url, connectionDetails.getUsername(),
                passwordEnDecryptionService.decrypt(connectionDetails.getPassword()));
    }

    private String getDbUrl(ConnectionDetails connectionDetails) {
        return String.format(JDBC_POSTGRESQL_CONNECTION_STRING,
                connectionDetails.getHostname(), connectionDetails.getPort(), connectionDetails.getDatabaseName());
    }
}
