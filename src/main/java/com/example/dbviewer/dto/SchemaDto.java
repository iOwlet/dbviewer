package com.example.dbviewer.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SchemaDto {

    private String name;
    private String catalog;
}
