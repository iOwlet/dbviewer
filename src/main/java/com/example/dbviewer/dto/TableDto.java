package com.example.dbviewer.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TableDto {

    private String name;
    private String schema;
    private String catalog;
    private String type;
    private String identifier;
}
