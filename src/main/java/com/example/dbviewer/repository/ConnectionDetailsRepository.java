package com.example.dbviewer.repository;

import com.example.dbviewer.entity.ConnectionDetails;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConnectionDetailsRepository extends PagingAndSortingRepository<ConnectionDetails, Long> {
}
