package com.example.dbviewer.controller;

import com.example.dbviewer.dto.ConnectionDetailsDto;
import com.example.dbviewer.entity.ConnectionDetails;
import com.example.dbviewer.mapper.ConnectionDetailsMapper;
import com.example.dbviewer.service.ConnectionDetailsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = ConnectionDetailsController.class)
class ConnectionDetailsControllerImplWebMvcTest {

    public static final long CONNECTION_ID = 123L;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ConnectionDetailsMapper connectionDetailsMapper;

    @MockBean
    private ConnectionDetailsService connectionDetailsService;

    ConnectionDetails connectionDetails;
    ConnectionDetailsDto connectionDetailsDto;

    @BeforeEach
    void setUp() {
        connectionDetails = ConnectionDetails.builder()
                .id(CONNECTION_ID)
                .name("my_lovely_db")
                .databaseName("postgres")
                .hostname("localhost")
                .port(5432)
                .username("postgres")
                .password("09c069a257334896bf1a0b61d3e9f65c5ccef6439a40f6ca07878ec504f29a60")
                .build();

        connectionDetailsDto = ConnectionDetailsDto.builder()
                .name("my_lovely_db")
                .databaseName("postgres")
                .hostname("localhost")
                .port(5432)
                .username("postgres")
                .password("welcome1")
                .build();

        when(connectionDetailsMapper.toDto(any(ConnectionDetails.class))).thenCallRealMethod();
        when(connectionDetailsMapper.toDtos(any(Page.class))).thenCallRealMethod();
        when(connectionDetailsMapper.toEntity(any(ConnectionDetailsDto.class))).thenReturn(connectionDetails);
    }

    @AfterEach
    void tearDown() {
        reset(connectionDetailsMapper, connectionDetailsService);
    }

    @Test
    void testGetConnectionList() throws Exception {
        when(connectionDetailsService.getConnectionList(any()))
                .thenReturn(new PageImpl<>(Collections.singletonList(connectionDetails)));

        mockMvc.perform(get("/api/connection/list")
                .param("page", "0")
                .param("size", "10")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.empty").value(false))
                .andExpect(jsonPath("$.totalElements").value(1))
                .andExpect(jsonPath("$.content[0].id").value(connectionDetails.getId()))
                .andExpect(jsonPath("$.content[0].name").value(connectionDetails.getName()))
                .andExpect(jsonPath("$.content[0].databaseName").value(connectionDetails.getDatabaseName()))
                .andExpect(jsonPath("$.content[0].hostname").value(connectionDetails.getHostname()))
                .andExpect(jsonPath("$.content[0].port").value(connectionDetails.getPort()))
                .andExpect(jsonPath("$.content[0].username").value(connectionDetails.getUsername()))
                .andExpect(jsonPath("$.content[0].password").value(connectionDetails.getPassword()));

        verify(connectionDetailsService, times(1)).getConnectionList(any());
        verify(connectionDetailsMapper, times(1)).toDtos(any(Page.class));
    }

    @Test
    void testGetConnection() throws Exception {
        when(connectionDetailsService.getConnection(anyLong())).thenReturn(connectionDetails);

        mockMvc.perform(get("/api/connection/{id}", CONNECTION_ID))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(connectionDetails.getId()))
                .andExpect(jsonPath("$.name").value(connectionDetails.getName()))
                .andExpect(jsonPath("$.databaseName").value(connectionDetails.getDatabaseName()))
                .andExpect(jsonPath("$.hostname").value(connectionDetails.getHostname()))
                .andExpect(jsonPath("$.port").value(connectionDetails.getPort()))
                .andExpect(jsonPath("$.username").value(connectionDetails.getUsername()))
                .andExpect(jsonPath("$.password").value(connectionDetails.getPassword()));

        verify(connectionDetailsService, times(1)).getConnection(any());
        verify(connectionDetailsMapper, times(1)).toDto(any(ConnectionDetails.class));
    }

    @Test
    void testSuccessSave() throws Exception {
        when(connectionDetailsService.save(any())).thenReturn(connectionDetails);

        mockMvc.perform(post("/api/connection/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(connectionDetailsDto)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(CONNECTION_ID))
                .andExpect(jsonPath("$.name").value(connectionDetails.getName()))
                .andExpect(jsonPath("$.databaseName").value(connectionDetails.getDatabaseName()))
                .andExpect(jsonPath("$.hostname").value(connectionDetails.getHostname()))
                .andExpect(jsonPath("$.port").value(connectionDetails.getPort()))
                .andExpect(jsonPath("$.username").value(connectionDetails.getUsername()))
                .andExpect(jsonPath("$.password").value(connectionDetails.getPassword()));

        verify(connectionDetailsService, times(1)).save(any());
        verify(connectionDetailsMapper, times(1)).toDto(any(ConnectionDetails.class));
        verify(connectionDetailsMapper, times(1)).toEntity(any(ConnectionDetailsDto.class));
    }

    @Test
    void testSuccessUpdate() throws Exception {
        when(connectionDetailsService.update(anyLong(), any())).thenReturn(connectionDetails);

        mockMvc.perform(put("/api/connection/{id}", CONNECTION_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(connectionDetailsDto)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(CONNECTION_ID))
                .andExpect(jsonPath("$.name").value(connectionDetails.getName()))
                .andExpect(jsonPath("$.databaseName").value(connectionDetails.getDatabaseName()))
                .andExpect(jsonPath("$.hostname").value(connectionDetails.getHostname()))
                .andExpect(jsonPath("$.port").value(connectionDetails.getPort()))
                .andExpect(jsonPath("$.username").value(connectionDetails.getUsername()))
                .andExpect(jsonPath("$.password").value(connectionDetails.getPassword()));

        verify(connectionDetailsService, times(1)).update(anyLong(), any());
        verify(connectionDetailsMapper, times(1)).toDto(any(ConnectionDetails.class));
        verify(connectionDetailsMapper, times(1)).toEntity(any(ConnectionDetailsDto.class));
    }

    @Test
    void testSuccessDelete() throws Exception {
        doNothing().when(connectionDetailsService).delete(anyLong());

        mockMvc.perform(delete("/api/connection/{id}", CONNECTION_ID))
                .andExpect(status().isOk());
    }

    @Test
    void testSaveValidations() throws Exception {
        mockMvc.perform(post("/api/connection/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(ConnectionDetailsDto.builder().build())))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testUpdateValidations() throws Exception {
        mockMvc.perform(post("/api/connection/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(ConnectionDetailsDto.builder().build())))
                .andExpect(status().isBadRequest());
    }
}