package com.example.dbviewer.service;

import com.example.dbviewer.dto.ColumnDto;
import com.example.dbviewer.dto.SchemaDto;
import com.example.dbviewer.dto.TableDto;

import java.util.List;
import java.util.Map;

public interface DbConnectionService {
    List<SchemaDto> getSchemas(Long connectionId);

    List<TableDto> getTables(Long connectionId, String schema);

    List<ColumnDto> getColumns(Long connectionId, String schema, String table);

    List<Map<String, String>> getData(Long connectionId, String schema, String table);
}
