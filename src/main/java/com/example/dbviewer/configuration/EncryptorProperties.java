package com.example.dbviewer.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "encryptor")
@Data
public class EncryptorProperties {

    private String password = "";
    private String salt = "5c0744940b5c369b";

}
