package com.example.dbviewer.service.impl;

import com.example.dbviewer.entity.ConnectionDetails;
import com.example.dbviewer.exception.ConnectionNotFoundException;
import com.example.dbviewer.repository.ConnectionDetailsRepository;
import com.example.dbviewer.service.ConnectionDetailsService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ConnectionDetailsServiceImpl implements ConnectionDetailsService {

    private final ConnectionDetailsRepository connectionDetailsRepository;

    @Override
    public Page<ConnectionDetails> getConnectionList(Pageable pageable) {
        return connectionDetailsRepository.findAll(pageable);
    }

    @Override
    public ConnectionDetails getConnection(Long id) {
        return connectionDetailsRepository.findById(id).orElseThrow(ConnectionNotFoundException::new);
    }

    @Override
    public ConnectionDetails save(ConnectionDetails connectionDetails) {
        return connectionDetailsRepository.save(connectionDetails);
    }

    @Override
    public ConnectionDetails update(Long id, ConnectionDetails connectionDetails) {
        ConnectionDetails existedConnection = getConnection(id);
        connectionDetails.setId(existedConnection.getId());
        return connectionDetailsRepository.save(connectionDetails);
    }

    @Override
    public void delete(Long id) {
        connectionDetailsRepository.deleteById(id);
    }
}
