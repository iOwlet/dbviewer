package com.example.dbviewer.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ColumnDto {

    private String name;
    private String table;
    private String schema;
    private String catalog;
    private String dataType;
    private String typeName;
    private String columnSize;
    private String decimalDigits;
    private String numPrecRadix;
    private String nullable;
    private String isAutoincrement;
    private String isGeneratedColumn;
}
