package com.example.dbviewer.controller;

import com.example.dbviewer.dto.ConnectionDetailsDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/api/connection")
public interface ConnectionDetailsController {

    @GetMapping("/list")
    Page<ConnectionDetailsDto> getConnectionList(Pageable pageable);

    @GetMapping("/{id}")
    ConnectionDetailsDto getConnection(@PathVariable Long id);

    @PostMapping("/")
    ConnectionDetailsDto save(@RequestBody ConnectionDetailsDto connectionDetailsDto);

    @PutMapping("/{id}")
    ConnectionDetailsDto update(@PathVariable Long id,
                                @RequestBody ConnectionDetailsDto connectionDetailsDto);

    @DeleteMapping("/{id}")
    void delete(@PathVariable Long id);
}
