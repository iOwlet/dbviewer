package com.example.dbviewer.controller.impl;

import com.example.dbviewer.controller.ConnectionDetailsController;
import com.example.dbviewer.dto.ConnectionDetailsDto;
import com.example.dbviewer.entity.ConnectionDetails;
import com.example.dbviewer.mapper.ConnectionDetailsMapper;
import com.example.dbviewer.service.ConnectionDetailsService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
public class ConnectionDetailsControllerImpl implements ConnectionDetailsController {

    private final ConnectionDetailsService connectionDetailsService;
    private final ConnectionDetailsMapper connectionDetailsMapper;

    @Override
    public Page<ConnectionDetailsDto> getConnectionList(Pageable pageable) {
        return connectionDetailsMapper.toDtos(connectionDetailsService.getConnectionList(pageable));
    }

    @Override
    public ConnectionDetailsDto getConnection(Long id) {
        return connectionDetailsMapper.toDto(connectionDetailsService.getConnection(id));
    }

    @Override
    public ConnectionDetailsDto save(@Valid ConnectionDetailsDto connectionDetailsDto) {
        ConnectionDetails savedConnectionDetails = connectionDetailsService.save(
                connectionDetailsMapper.toEntity(connectionDetailsDto));
        return connectionDetailsMapper.toDto(savedConnectionDetails);
    }

    @Override
    public ConnectionDetailsDto update(Long id, @Valid ConnectionDetailsDto connectionDetailsDto) {
        ConnectionDetails savedConnectionDetails = connectionDetailsService.update(id,
                connectionDetailsMapper.toEntity(connectionDetailsDto));
        return connectionDetailsMapper.toDto(savedConnectionDetails);
    }

    @Override
    public void delete(Long id) {
        connectionDetailsService.delete(id);
    }
}
