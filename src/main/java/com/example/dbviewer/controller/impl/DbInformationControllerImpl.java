package com.example.dbviewer.controller.impl;

import com.example.dbviewer.controller.DbInformationController;
import com.example.dbviewer.dto.ColumnDto;
import com.example.dbviewer.dto.SchemaDto;
import com.example.dbviewer.dto.TableDto;
import com.example.dbviewer.service.impl.DbConnectionServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@AllArgsConstructor
public class DbInformationControllerImpl implements DbInformationController {

    private final DbConnectionServiceImpl dbConnectionService;

    @Override
    public List<SchemaDto> getSchemas(Long connectionId) {
        return dbConnectionService.getSchemas(connectionId);
    }

    @Override
    public List<TableDto> getTables(Long connectionId, String schema) {
        return dbConnectionService.getTables(connectionId, schema);
    }

    @Override
    public List<ColumnDto> getColumns(Long connectionId, String schema, String table) {
        return dbConnectionService.getColumns(connectionId, schema, table);
    }

    @Override
    public List<Map<String, String>> getDataPreview(Long connectionId, String schema, String table) {
        return dbConnectionService.getData(connectionId, schema, table);
    }

}
