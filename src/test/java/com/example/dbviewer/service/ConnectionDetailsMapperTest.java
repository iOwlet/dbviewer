package com.example.dbviewer.service;

import com.example.dbviewer.configuration.EncryptorProperties;
import com.example.dbviewer.dto.ConnectionDetailsDto;
import com.example.dbviewer.entity.ConnectionDetails;
import com.example.dbviewer.mapper.ConnectionDetailsMapper;
import com.example.dbviewer.service.impl.PasswordEnDecryptionServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ConnectionDetailsMapperTest {

    ConnectionDetailsMapper connectionDetailsMapper;

    @BeforeEach
    void setUp() {
        connectionDetailsMapper = new ConnectionDetailsMapper(
                new PasswordEnDecryptionServiceImpl(
                        new EncryptorProperties()));
    }

    @Test
    void transformNullDtoToModel() {
        ConnectionDetailsDto connectionDetailsDto = null;
        Assertions.assertThrows(NullPointerException.class,
                () -> connectionDetailsMapper.toEntity(connectionDetailsDto));
    }

    @Test
    void transformEmptyDtoToModel() {
        ConnectionDetailsDto connectionDetailsDto = ConnectionDetailsDto.builder().build();
        Assertions.assertThrows(NullPointerException.class,
                () -> connectionDetailsMapper.toEntity(connectionDetailsDto));
    }

    @Test
    void transformSimpleDtoToModel() {
        ConnectionDetailsDto connectionDetailsDto = ConnectionDetailsDto.builder()
                .id(1L)
                .name("marvel_universe")
                .databaseName("marvel")
                .hostname("localhost")
                .port(5432)
                .username("postgres")
                .password("welcome1")
                .build();

        ConnectionDetails connectionDetails = connectionDetailsMapper.toEntity(connectionDetailsDto);

        Assertions.assertEquals(connectionDetailsDto.getId(), connectionDetails.getId());
        Assertions.assertEquals(connectionDetailsDto.getUserId(), connectionDetails.getUserId());
        Assertions.assertEquals(connectionDetailsDto.getDatabaseName(), connectionDetails.getDatabaseName());
        Assertions.assertEquals(connectionDetailsDto.getName(), connectionDetails.getName());
        Assertions.assertEquals(connectionDetailsDto.getHostname(), connectionDetails.getHostname());
        Assertions.assertEquals(connectionDetailsDto.getPort(), connectionDetails.getPort());
        Assertions.assertEquals(connectionDetailsDto.getUsername(), connectionDetails.getUsername());
        Assertions.assertNotEquals(connectionDetailsDto.getPassword(), connectionDetails.getPassword());
    }
}