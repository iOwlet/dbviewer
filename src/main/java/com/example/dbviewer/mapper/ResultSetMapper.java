package com.example.dbviewer.mapper;

import com.example.dbviewer.dto.ColumnDto;
import com.example.dbviewer.dto.SchemaDto;
import com.example.dbviewer.dto.TableDto;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Component
public class ResultSetMapper {

    public SchemaDto mapSchemaDto(ResultSet schemasData) throws SQLException {
        return SchemaDto.builder()
                .catalog(schemasData.getString("TABLE_CATALOG"))
                .name(schemasData.getString("TABLE_SCHEM"))
                .build();
    }

    public TableDto mapTableDto(ResultSet tablesData) throws SQLException {
        return TableDto.builder()
                .catalog(tablesData.getString("TABLE_CAT"))
                .schema(tablesData.getString("TABLE_SCHEM"))
                .name(tablesData.getString("TABLE_NAME"))
                .type(tablesData.getString("TABLE_TYPE"))
                .identifier(tablesData.getString("SELF_REFERENCING_COL_NAME"))
                .build();
    }

    public ColumnDto mapColumnDto(ResultSet tablesData) throws SQLException {
        return ColumnDto.builder()
                .catalog(tablesData.getString("TABLE_CAT"))
                .schema(tablesData.getString("TABLE_SCHEM"))
                .table(tablesData.getString("TABLE_NAME"))
                .name(tablesData.getString("COLUMN_NAME"))
                .dataType(tablesData.getString("DATA_TYPE"))
                .typeName(tablesData.getString("TYPE_NAME"))
                .columnSize(tablesData.getString("COLUMN_SIZE"))
                .decimalDigits(tablesData.getString("DECIMAL_DIGITS"))
                .numPrecRadix(tablesData.getString("NUM_PREC_RADIX"))
                .nullable(tablesData.getString("NULLABLE"))
                .isAutoincrement(tablesData.getString("IS_AUTOINCREMENT"))
                .isGeneratedColumn(tablesData.getString("IS_GENERATEDCOLUMN"))
                .build();
    }

    public Map<String, String> mapTableData(ResultSet data) throws SQLException {
        Map<String, String> rowData = new HashMap<>();
        int columnCount = data.getMetaData().getColumnCount();

        for (int i = 0; i < columnCount; i++) {
            String columnName = data.getMetaData().getColumnName(i + 1);
            String value = data.getString(i + 1);
            rowData.put(columnName, value);
        }
        return rowData;
    }
}
