package com.example.dbviewer.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = ConnectionDetails.CONNECTION_DETAILS_TABLE_NAME)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ConnectionDetails {

    public static final String CONNECTION_DETAILS_TABLE_NAME = "connection_details";

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String hostname;

    @Column
    private Integer port;

    @Column
    private String databaseName;

    @Column
    private String username;

    @Column
    private String password;

    @Column
    private final long userId = 0L;
}
