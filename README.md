DB Viewer REST API

Supported DB: Postgres

Swagger documentation: http://localhost:8080/swagger-ui/index.html

What could be improved:
1. Spring Security + Users
2. Tests for DbInformationController flow
3. Failed cases: exceptions, tests
4. Liquibase
5. Spring configuration password encryption
6. Gitlab CI