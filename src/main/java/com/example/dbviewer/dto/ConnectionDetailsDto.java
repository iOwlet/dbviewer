package com.example.dbviewer.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ConnectionDetailsDto {

    private Long id;

    @Size(max = 255)
    private String name;

    @NotEmpty
    @Size(max = 255)
    private String hostname;

    @NotNull
    @Min(0)
    private Integer port;

    @NotEmpty
    @Size(max = 255)
    private String databaseName;

    @NotEmpty
    @Size(max = 255)
    private String username;

    private String password;

    private long userId;
}
