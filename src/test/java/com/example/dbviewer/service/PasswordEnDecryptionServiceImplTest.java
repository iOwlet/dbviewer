package com.example.dbviewer.service;

import com.example.dbviewer.configuration.EncryptorProperties;
import com.example.dbviewer.service.impl.PasswordEnDecryptionServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class PasswordEnDecryptionServiceImplTest {

    PasswordEnDecryptionService passwordEnDecryptionService;

    @BeforeEach
    void setUp() {
        passwordEnDecryptionService = new PasswordEnDecryptionServiceImpl(new EncryptorProperties());
    }

    @Test
    void testDecryptEncryptedPassword() {
        String password = "password";

        String encryptedPassword = passwordEnDecryptionService.encrypt(password);

        assertNotEquals(password, encryptedPassword);

        String decryptedPassword = passwordEnDecryptionService.decrypt(encryptedPassword);

        assertEquals(password, decryptedPassword);
    }

}