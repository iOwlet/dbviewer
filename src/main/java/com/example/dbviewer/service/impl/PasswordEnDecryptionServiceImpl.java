package com.example.dbviewer.service.impl;

import com.example.dbviewer.configuration.EncryptorProperties;
import com.example.dbviewer.service.PasswordEnDecryptionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PasswordEnDecryptionServiceImpl implements PasswordEnDecryptionService {

    private final TextEncryptor textEncryptor;

    public PasswordEnDecryptionServiceImpl(EncryptorProperties encryptorProperties) {
        this.textEncryptor = Encryptors.text(encryptorProperties.getPassword(), encryptorProperties.getSalt());
    }

    @Override
    public String encrypt(String password) {
        String encryptedPassword = textEncryptor.encrypt(password);
        log.debug("Encrypted = {}", encryptedPassword);
        return encryptedPassword;
    }

    @Override
    public String decrypt(String encryptedPassword) {
        log.debug("Encrypted = {}", encryptedPassword);
        return textEncryptor.decrypt(encryptedPassword);
    }
}
