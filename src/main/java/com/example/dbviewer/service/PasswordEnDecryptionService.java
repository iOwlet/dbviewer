package com.example.dbviewer.service;

public interface PasswordEnDecryptionService {

    String encrypt(String password);

    String decrypt(String encodedPassword);
}
