package com.example.dbviewer.mapper;

import com.example.dbviewer.dto.ConnectionDetailsDto;
import com.example.dbviewer.entity.ConnectionDetails;
import com.example.dbviewer.service.PasswordEnDecryptionService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
@Slf4j
public class ConnectionDetailsMapper {

    private final PasswordEnDecryptionService enDecryptionService;

    public Page<ConnectionDetailsDto> toDtos(Page<ConnectionDetails> connections) {
        List<ConnectionDetailsDto> list = connections.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
        return new PageImpl<>(list, connections.getPageable(), connections.getTotalPages());
    }

    public ConnectionDetailsDto toDto(ConnectionDetails connectionDetails) {
        return ConnectionDetailsDto.builder()
                .id(connectionDetails.getId())
                .name(connectionDetails.getName())
                .hostname(connectionDetails.getHostname())
                .port(connectionDetails.getPort())
                .databaseName(connectionDetails.getDatabaseName())
                .username(connectionDetails.getUsername())
                .password(connectionDetails.getPassword())
                .userId(connectionDetails.getUserId())
                .build();
    }

    public ConnectionDetails toEntity(ConnectionDetailsDto connectionDetailsDto) {
        return ConnectionDetails.builder()
                .id(connectionDetailsDto.getId())
                .name(connectionDetailsDto.getName())
                .hostname(connectionDetailsDto.getHostname())
                .port(connectionDetailsDto.getPort())
                .databaseName(connectionDetailsDto.getDatabaseName())
                .username(connectionDetailsDto.getUsername())
                .password(enDecryptionService.encrypt(connectionDetailsDto.getPassword()))
                .build();
    }
}
