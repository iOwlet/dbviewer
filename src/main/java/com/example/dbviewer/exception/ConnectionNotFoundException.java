package com.example.dbviewer.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Connection not found")
public class ConnectionNotFoundException extends RuntimeException {
}
