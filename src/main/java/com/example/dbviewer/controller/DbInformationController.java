package com.example.dbviewer.controller;

import com.example.dbviewer.dto.ColumnDto;
import com.example.dbviewer.dto.SchemaDto;
import com.example.dbviewer.dto.TableDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

@RequestMapping("/api/information")
public interface DbInformationController {

    @GetMapping("/{connectionId}/schemas")
    List<SchemaDto> getSchemas(@PathVariable Long connectionId);

    @GetMapping("/{connectionId}/{schema}/tables")
    List<TableDto> getTables(@PathVariable Long connectionId,
                             @PathVariable String schema);

    @GetMapping("/{connectionId}/{schema}/{table}/columns")
    List<ColumnDto> getColumns(@PathVariable Long connectionId,
                               @PathVariable String schema,
                               @PathVariable String table);

    @GetMapping("/{connectionId}/{schema}/{table}/data")
    List<Map<String, String>> getDataPreview(@PathVariable Long connectionId,
                                             @PathVariable String schema,
                                             @PathVariable String table);

}
